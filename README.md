# Roster Validator

Validates a roster of players as a code task. It's a CLI tool implemented in cobra. To run it, first build it with `$ make build` and then run the program with `$ roster-validator -f <filename>`.
