//nolint:gochecknoinits
// This package is well recognized as an excellent CLI package despite following different conventions
// that the ones I prefer to use, therefore I will skip linting this.
package cmd

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"github.com/spf13/cobra"

	"gitlab.com/pbabbicola/roster-validator/pkg/roster"
)

var rosterFile string // This may seem a bit unorthodox but it's how you normally work with this package.
var logger *log.Logger

var rootCmd = &cobra.Command{
	Use:   "roster-validator",
	Short: "Validates a roster of players.",
	Long:  `Validates a roster of players with a certain preset rules.`,
	Run: func(cmd *cobra.Command, args []string) {
		if err := validateRoster(rosterFile); err != nil {
			logger.Fatalf("Error - %v", err) // Formating a bit with a separator
		}
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
// In this case, we don't have any child commands.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	rootCmd.Flags().StringVarP(&rosterFile, "file", "f", "", "roster configuration in JSON format")
	logger = log.New(os.Stdout, "", log.Ltime)
	if err := rootCmd.MarkFlagRequired("file"); err != nil {
		logger.Fatalf("Error - Failed to set up, please contact the developer.")
	}
}

func validateRoster(filename string) error {
	filename = strings.TrimSpace(filename)
	if filename == "" {
		return fmt.Errorf("filename cannot be empty")
	}
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			return fmt.Errorf("file does not exist")
		}
		return fmt.Errorf("failed to read file: %w", err)
	}
	var r roster.Roster
	// So, here is a thing. json.Unmarshal will ignore fields that don't exist in the struct.
	// Personally, I don't think it's a bad behavior, but if you wanted to avoid it you could
	// use a custom json.Decoder with DisallowUnknownFields().
	if e := json.Unmarshal(b, &r); e != nil {
		return fmt.Errorf("failed to unmarshal json file: %w", e)
	}
	if e := r.Validate(); e != nil {
		return fmt.Errorf("invalid roster: %w", e)
	}
	return nil
}
