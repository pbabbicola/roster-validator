BIN_NAME=roster_validator
CMD=cmd/main.go

all: test build
build: 
	go build -o $(BIN_NAME) -v $(CMD)
test: 
	go test -v ./...
race:
	go test -race ./...
msan:
	go test -msan ./...
lint:
	golangci-lint run $(go list ./...)
clean: 
	go clean
	rm -f $(BIN_NAME)
run:
	go build -o $(BIN_NAME) -v $(CMD)
	./$(BIN_NAME)
