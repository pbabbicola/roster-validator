package main

import "gitlab.com/pbabbicola/roster-validator/cmd/cmd"

func main() {
	cmd.Execute()
}
