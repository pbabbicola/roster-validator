package roster_test

import (
	"encoding/json"
	"errors"
	"testing"
	"time"

	"gitlab.com/pbabbicola/roster-validator/pkg/datehelper"
	"gitlab.com/pbabbicola/roster-validator/pkg/roster"
)

func Test_RoleString(t *testing.T) {
	role := roster.IGL
	expected := "igl"
	if string(role) != expected {
		t.Fatalf("Role is equal to %v. Expected: %v", role, expected)
	}

	role = roster.AWPer
	expected = "awper"
	if string(role) != expected {
		t.Fatalf("Role is equal to %v. Expected: %v", role, expected)
	}

	role = roster.Rifler
	expected = "rifler"
	if string(role) != expected {
		t.Fatalf("Role is equal to %v. Expected: %v", role, expected)
	}

	role = roster.Support
	expected = "support"
	if string(role) != expected {
		t.Fatalf("Role is equal to %v. Expected: %v", role, expected)
	}

	role = "some error"
	expectedErr := roster.ErrInvalidRole
	if !errors.Is(role.IsValid(), expectedErr) {
		t.Fatalf("Err is equal to %v. ExpectedErr: %v", role.IsValid(), expectedErr)
	}
}

func Test_RoleUnmarshaing(t *testing.T) {
	var r struct {
		Role roster.Role `json:"role"`
	}
	if err := json.Unmarshal([]byte(`{"role":"igl"}`), &r); err != nil {
		t.Fatal(err)
	}
	if r.Role != roster.IGL {
		t.Fatalf("unmarshaled incorrectly, expected %v, got %v", roster.IGL, r.Role)
	}

	if err := json.Unmarshal([]byte(`{"role":"awper"}`), &r); err != nil {
		t.Fatal(err)
	}
	if r.Role != roster.AWPer {
		t.Fatalf("unmarshaled incorrectly, expected %v, got %v", roster.AWPer, r.Role)
	}

	if err := json.Unmarshal([]byte(`{"role":"rifler"}`), &r); err != nil {
		t.Fatal(err)
	}
	if r.Role != roster.Rifler {
		t.Fatalf("unmarshaled incorrectly, expected %v, got %v", roster.Rifler, r.Role)
	}

	if err := json.Unmarshal([]byte(`{"role":"support"}`), &r); err != nil {
		t.Fatal(err)
	}
	if r.Role != roster.Support {
		t.Fatalf("unmarshaled incorrectly, expected %v, got %v", roster.Support, r.Role)
	}
}

func TestPlayerUnmarshaling(t *testing.T) {
	var r roster.Player
	if err := json.Unmarshal([]byte(`{"role":"igl"}`), &r); err != nil {
		t.Fatal(err)
	}
	if r.Role != roster.IGL {
		t.Fatalf("unmarshaled incorrectly, expected %v, got %v", roster.IGL, r.Role)
	}

	if err := json.Unmarshal([]byte(`{"role":"awper"}`), &r); err != nil {
		t.Fatal(err)
	}
	if r.Role != roster.AWPer {
		t.Fatalf("unmarshaled incorrectly, expected %v, got %v", roster.AWPer, r.Role)
	}

	if err := json.Unmarshal([]byte(`{"role":"rifler"}`), &r); err != nil {
		t.Fatal(err)
	}
	if r.Role != roster.Rifler {
		t.Fatalf("unmarshaled incorrectly, expected %v, got %v", roster.Rifler, r.Role)
	}

	if err := json.Unmarshal([]byte(`{"role":"support"}`), &r); err != nil {
		t.Fatal(err)
	}
	if r.Role != roster.Support {
		t.Fatalf("unmarshaled incorrectly, expected %v, got %v", roster.Support, r.Role)
	}
}

func Test_UnmarshalFullPlayer(t *testing.T) {
	var r roster.Player
	if err := json.Unmarshal([]byte(`{
		"id": 337332768876789763,
		"first_name": "Jane",
		"last_name": "Beddingfield",
		"alias": "__Jain",
		"dob": "1993-05-18T00:00:00",
		"role": "support",
		"active": true
	}`), &r); err != nil {
		t.Fatal(err)
	}
	expected := roster.Player{
		Active:    true,
		Role:      roster.Support,
		Alias:     "__Jain",
		DOB:       datehelper.Time(time.Date(1993, time.May, 18, 0, 0, 0, 0, time.UTC)),
		FirstName: "Jane",
		LastName:  "Beddingfield",
		ID:        337332768876789763,
	}

	if r.Role != expected.Role {
		t.Fatalf("unmarshaled incorrectly, expected role %v, got %v", expected.Role, r.Role)
	}
	if r.ID != expected.ID {
		t.Fatalf("unmarshaled incorrectly, expected id %v, got %v", expected.ID, r.ID)
	}
	if r.Alias != expected.Alias {
		t.Fatalf("unmarshaled incorrectly, expected alias %v, got %v", expected.Alias, r.Alias)
	}
	if r.DOB != expected.DOB {
		t.Fatalf("unmarshaled incorrectly, expected dob %v, got %v", expected.DOB, r.DOB)
	}
	if r.FirstName != expected.FirstName {
		t.Fatalf("unmarshaled incorrectly, expected first name %v, got %v", expected.FirstName, r.FirstName)
	}
	if r.LastName != expected.LastName {
		t.Fatalf("unmarshaled incorrectly, expected last name %v, got %v", expected.LastName, r.LastName)
	}
}
