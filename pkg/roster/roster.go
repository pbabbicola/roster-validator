package roster

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"
	"time"

	"gitlab.com/pbabbicola/roster-validator/pkg/datehelper"
)

var ErrInvalidRole = errors.New("the provided role is invalid")

type Role string

const (
	Rifler  Role = "rifler"
	IGL     Role = "igl"
	Support Role = "support"
	AWPer   Role = "awper"
) // I am not sure how often the Counter Strike meta changes, but
// I assume you have given me a tiny subset of the possible roles.
// Another option would have been to make it configurable but I considered
// that for the code challenge it was not necessary.

func (r Role) IsValid() error {
	switch r {
	case Rifler, IGL, Support, AWPer:
		return nil
	}

	return fmt.Errorf("%w: role found: %v", ErrInvalidRole, r)
}

// UnmarshalJSON is a custom unmarshaling for Role that also validates the "enum"
func (r *Role) UnmarshalJSON(b []byte) error {
	type R Role

	role := (*R)(r)
	err := json.Unmarshal(b, &role)

	if err != nil {
		return fmt.Errorf("failed to unmarshal role: %w", err)
	}
	*role = R(strings.ToLower(string(*role)))

	return r.IsValid()
}

type Player struct {
	ID        uint64          `json:"id"`
	FirstName string          `json:"first_name"`
	LastName  string          `json:"last_name"`
	Alias     string          `json:"alias"`
	Role      Role            `json:"role"`
	DOB       datehelper.Time `json:"dob"`
	Active    bool            `json:"active"`
}

type Roster struct {
	ID      uint64   `json:"id"`
	Players []Player `json:"players"`
}

func (r Roster) Validate() error {
	if r.ID == 0 {
		return fmt.Errorf("roster id cannot be empty")
	}
	// I did not find much regarding validation of these IDs, only that they are uint64s
	var c int
	for _, p := range r.Players {
		if p.ID == 0 {
			return fmt.Errorf("id cannot be empty")
		}
		if p.FirstName == "" {
			return fmt.Errorf("first name cannot be empty")
		}
		if p.LastName == "" {
			return fmt.Errorf("last name cannot be empty")
		}
		if p.Alias == "" {
			return fmt.Errorf("alias cannot be empty")
		}
		if (time.Time(p.DOB)).IsZero() {
			return fmt.Errorf("date of birth cannot be empty")
		}
		if p.Active {
			c++
		}
	}
	if c != 5 { // again, this could be configurable
		return fmt.Errorf("a roster can only have exactly 5 active players, but this one has %v", c)
	}
	return nil
}
