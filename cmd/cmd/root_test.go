//nolint:gochecknoinits
// This package is well recognized as an excellent CLI package despite following different conventions
// that the ones I prefer to use, therefore I will skip linting this.
package cmd

import (
	"testing"
)

func TestRun(t *testing.T) {
	tests := []struct {
		name        string
		filename    string
		expectedErr bool
	}{
		{
			name:        "provided file",
			filename:    "testdata/provided.json",
			expectedErr: true,
		},
		{
			name:        "does not exist",
			filename:    "testdata/asddasd.json",
			expectedErr: true,
		},
		{
			name:        "wrong format",
			filename:    "testdata/wrongformat.json",
			expectedErr: true,
		},
		{
			name:        "empty field",
			filename:    "testdata/emptyfield.json",
			expectedErr: true,
		},
		{
			name:        "wrong amount of players",
			filename:    "testdata/wrongamount.json",
			expectedErr: true,
		},
		{
			name:        "valid",
			filename:    "testdata/valid.json",
			expectedErr: false,
		},
	}
	for _, tt := range tests {
		testCase := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			if err := validateRoster(testCase.filename); (err != nil) != testCase.expectedErr {
				t.Fatalf("Error expected: %v, but got: %v", testCase.expectedErr, err)
			}
		})
	}
}
