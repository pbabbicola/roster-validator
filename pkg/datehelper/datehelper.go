package datehelper

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"
)

type Time time.Time

// UnmarshalJSON is a custom unmarshaling for Time (which is equal to time.Time)
// that parses and validates time in ISO8601
func (t *Time) UnmarshalJSON(b []byte) error {
	var tt string
	err := json.Unmarshal(b, &tt)
	if err != nil {
		return fmt.Errorf("failed to unmarshal time into a string: %w", err)
	}

	if !strings.Contains(tt, "Z") {
		// I'll just assume that this date is in ISO8601 instead of RFC3999 if this is the case.
		// If not, this will fail anyways,
		tt = fmt.Sprintf("%vZ", tt)
	}

	newTime, err := time.Parse(time.RFC3339, tt)
	*t = Time(newTime)
	return err
}
